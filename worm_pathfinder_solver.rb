require_relative 'worm_pathfinder'
require 'optparse'
require 'bzip2'

# 
# ===== BEGIN command-line parameters parse =====
# 

# Default options
options = {initial_vertex: 0, walk_limit: -1, worm_size: 5,
  verbose: false, pause: false}

ARGV << '-h' if ARGV.empty?
OptionParser.new do |opts|
  opts.banner = """
    Usage: ruby worm_pathfinder_solver.rb [OPTIONS]
    """

  opts.on("-i", "--input CSV_FILENAME", String,
          "Input file to read.",
          "Must be a CSV file and could be compressed or plain.",
          "Compresion format supported is Bzip2.") do |f|
    options[:input] = f
  end

  opts.on("-v", "--initial-vertex VERTEX", Integer,
          "Initial vertex to start random walk.",
          "Default: 0") do |v|
    options[:initial_vertex] = v
  end

  opts.on("-l", "--walk_limit STEPS", Integer,
          "Numbers of steps to walk into graph.",
          "This parameter should be a positive odd integer.",
          "Default: -1"
          ) do |l|
    options[:walk_limit] = l
  end

  opts.on("-s", "--worm-size SIZE", Integer,
          "Numbers of vertices of the worm.",
          "Default: 5") do |s|
    options[:worm_size] = s
  end

  opts.on("-d", "--induced_cycles_limit SIZE", Integer,
          "Numbers of vertices of the worm.",
          "Default: 5") do |s|
    options[:induced_cycles_limit] = s
  end

  opts.on("-p", "--[no-]pause",
          "Pause each induced cycle was found.",
          "Default: false") do |p|
    options[:pause] = p
  end

  ## FIXME: verbose and initial-vertex ambiguous flag
  opts.on("-V", "--[no-]verbose",
          "Print computation messages.",
          "Default: false") do |v|
    options[:verbose] = v
  end

  # opts.on("-o", "--output-prefix FILENAME", String,
  #         "Output file to write. Avoid extensions.") do |o|
  #   options[:output_prefix] = o
  # end

  # COMPRESS_ALGO = [:none, :bzip2]
  # opts.on("-c", "--compress [ALGORITHM]",
  #         COMPRESS_ALGO,
  #         "Compresion used for outputs. #{COMPRESS_ALGO.to_s}") do |c|
  #   options[:compress_algo] = c
  # end

  # No argument, shows at tail.  This will print an options summary.
  # Try it and see!
  opts.on_tail("-h", "--help", "Show this message") do
    puts opts
    exit
  end
end.parse!

raise ArgumentError, "Specify input file. See USAGE (-h)." if options[:input].nil?
raise ArgumentError, "Worm size must be odd. See USAGE (-h)." if options[:worm_size] % 2 == 0
# 
# ===== END command-line parameters parse =====
# 



# ===== BEGIN read input =====
pbar = ProgressBar.create(
  title: "Reading paths graph",
  starting_at: 1,
  total: nil)

edges = []
filename = options[:input]
if File.extname(filename).downcase == ".bz2"
  # Open streams to file
  File.open(filename, 'r') do |plain|
    Bzip2::Reader.open(plain) do |bzip2|
      CSV.parse(bzip2.read, converters: :numeric) do |row|
        edges << row
        pbar.increment
      end
    end
  end
else
  File.open(filename, "r") do |plain|
    CSV.foreach(plain, converters: :numeric) do |row|
      edges << row
      pbar.increment
    end
  end
end
pbar.stop

# ===== END read input =====

# ===== BEGIN random walk =====
WormPathfinder.random_walk(edges,options)
# ===== END random walk =====

