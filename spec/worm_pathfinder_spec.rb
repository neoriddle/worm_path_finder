require 'spec_helper'
require_relative '../worm_pathfinder'

describe "worm_pathfinder" do
  it "find neighbors" do
    v = (0...8).to_a
    e = [
      [0,1],[0,3],[0,4],[0,6],[0,7],
      [1,2],[1,4],[1,5],[1,6],
      [2,3],[2,5],
      [3,6],[3,7],
      [4,5],
      [5,7]
    ]
    find_neighbors(0,e).should == [1,3,4,6,7]
    find_neighbors(1,e).should == [0,2,4,5,6]
    find_neighbors(2,e).should == [1,3,5]
    find_neighbors(3,e).should == [0,2,6,7]
    find_neighbors(4,e).should == [0,1,5]
    find_neighbors(5,e).should == [1,2,4,7]
    find_neighbors(6,e).should == [0,1,3]
    find_neighbors(7,e).should == [0,3,5]
  end

  it "is_edge?" do
    i, j = 1,3
    edges = []
    is_edge?(i,j,edges).should be_false
    edges << [1,2]
    is_edge?(i,j,edges).should be_false
    edges << [3,1]
    is_edge?(i,j,edges).should be_true
    edges = [[1,3]]
    is_edge?(i,j,edges).should be_true
  end

  it "has_cycles?" do
    worm = [1,2,3,4,5]
    edges = [[1,2],[2,3],[3,4],[4,5]]
    has_cycles?(worm,edges).should be_false

    edges << [1,5]
    has_cycles?(worm,edges).should be_false

    edges << [1,4]
    has_cycles?(worm,edges).should be_true
  end

  it "compute_paths_graph" do
    paths = [
      [0, 32, 36, 52, 48, 16, 20, 4],
      [0, 2, 10, 14, 12, 28, 20, 4],
      [0, 8, 24, 28, 20, 21, 5, 4],
      [12, 44, 40, 56, 60, 28, 24, 8],
      [12, 14, 10, 42, 46, 44, 40, 8],
      [1, 3, 6, 7, 9, 11, 13, 15]
    ]
    
    edges = [[0,1],[0,2],[1,2],[1,3],[1,4],[2,3],[2,4],[3,4]]

    compute_paths_graph(paths).should == edges
  end

end