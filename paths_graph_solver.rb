require_relative 'worm_pathfinder'
require 'optparse'
require 'bzip2'




# 
# ===== BEGIN command-line parameters parse =====
# 
options = {}
ARGV << '-h' if ARGV.empty?
OptionParser.new do |opts|
  opts.banner = """
    Usage: ruby berge_graph_solver.rb -i CSV-FILENAME
    """

  opts.on("-i", "--input CSV_FILENAME", String,
          "Input csv file to read") do |n|
    options[:input] = n
  end

  opts.on("-o", "--output-prefix FILENAME", String,
          "Output file to write. Avoid extensions.") do |o|
    options[:output_prefix] = o
  end

  COMPRESS_ALGO = [:none, :bzip2]
  opts.on("-c", "--compress [ALGORITHM]",
          COMPRESS_ALGO,
          "Compresion used for outputs. #{COMPRESS_ALGO.to_s}") do |c|
    options[:compress_algo] = c
  end

  # No argument, shows at tail.  This will print an options summary.
  # Try it and see!
  opts.on_tail("-h", "--help", "Show this message") do
    puts opts
    exit
  end
end.parse!

raise ArgumentError, "Specify output file. See USAGE (-h)." if options[:output_prefix].nil?
# 
# ===== END command-line parameters parse =====
# 


# ===== BEGIN loading paths from CSV =====
paths = WormPathfinder.from_csv(options[:input])
# ===== END loading paths from CSV =====



# ===== BEGIN open streams =====
writer =  case options[:compress_algo]
          when :bzip2
            filename = options[:output_prefix] + ".csv.bz2"
            plain = File.open(filename, 'wb')
            bzip2 = Bzip2::Writer.new(plain)
            CSV(bzip2)
          else
            filename = options[:output_prefix] + ".csv"
            plain = File.open(filename, 'wb')
            CSV(plain)
          end
# ===== END open streams =====


# ===== BEGIN computing and storing paths graph =====
pbar = ProgressBar.create(
  title: "Computing paths graph",
  format: "%t [%B] [%c/%C - %P%%]",
  total: paths.size)
WormPathfinder.compute_paths_graph(paths, writer, progress_bar: pbar)
# ===== END computing and storing paths graph =====


# ===== BEGIN close streams =====
writer.close
# ===== END close streams =====

